package com.crud.react.repository;

import com.crud.react.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {
    @Query(value = "select * from cart where user_id = :user_id", nativeQuery = true)
    List<Cart> findByUser(int user_id);

//    Map<String, Object> checkoutProduct(int user_id);
//    Page<Cart> findByUser(User userId Pageable pageable);
}
