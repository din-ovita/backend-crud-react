package com.crud.react.repository;

import com.crud.react.model.Produk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProdukRepository extends JpaRepository<Produk, Integer> {
    @Query(value = "select * from produk where nama LIKE CONCAT('%',:nama, '%')", nativeQuery = true)
    Page<Produk> findByName(String nama, Pageable pageable);

}
