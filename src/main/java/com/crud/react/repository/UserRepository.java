package com.crud.react.repository;

import com.crud.react.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "select * from user where email = :email", nativeQuery = true)
    Optional<User> findByEmail(String email);
//    untuk mencari email

    Boolean existsByEmail(String email);
}
