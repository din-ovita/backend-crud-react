package com.crud.react.controller;

import com.crud.react.dto.CartDto;
import com.crud.react.model.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping
    public CommonResponse<Cart> addCart(@RequestBody CartDto cart) {
        return ResponseHelper.ok(cartService.addCart(cart));
    }

    @GetMapping
    public CommonResponse<List<Cart>> getAllCart(@RequestParam int userId) {
        return ResponseHelper.ok(cartService.getAllCart(userId));
    }

    @PutMapping("/{id}")
    public CommonResponse<Cart> update(@PathVariable("id") int id, @RequestBody CartDto cart) {
        return ResponseHelper.ok(cartService.update(id, cart));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Object>> delete(@PathVariable("id") int id) {
        return ResponseHelper.ok(cartService.delete(id));
    }

    @DeleteMapping
    public CommonResponse<Map<String, Boolean>> checkout(@RequestParam int userId) {
        return ResponseHelper.ok(cartService.checkout(userId));
    }}
