package com.crud.react.controller;

import com.crud.react.dto.ProdukDto;
import com.crud.react.model.Produk;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProdukService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/produk")
public class ProdukController {
    @Autowired
    private ProdukService produkService;
    @Autowired
    private ModelMapper modelMapper;
//    form-data tidak bisa menggunakan @RequestBody
    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Produk> addProduk(ProdukDto produkDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(produkService.addProduk(modelMapper.map(produkDto, Produk.class), multipartFile));
//        return ResponseHelper.ok(produkService.addProduk(produk, multipartFile));
    }
    @GetMapping("/{id}")
    public CommonResponse<Produk> getProdukId(@PathVariable("id") Integer id) {
        return ResponseHelper.ok(produkService.getProdukId(id));
    }
    @GetMapping("/all-produk")
    public CommonResponse<Page<Produk>> getAllProduk(@RequestParam(required = false) int page, @RequestParam(required = false) String nama) {
        return ResponseHelper.ok(produkService.getAllProduk(page, nama == null ? "" : nama));
    }
    @PutMapping(consumes = "multipart/form-data", path="/{id}")
    public CommonResponse<Produk> updateProduk(@PathVariable("id") Integer id, ProdukDto produk, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(produkService.updateProduk(id, modelMapper.map(produk, Produk.class), multipartFile));
    }
    //    @PutMapping("/{id}")
//    public CommonResponse<Produk> updateProduk(@PathVariable("id") Integer id, @RequestBody Produk produk) {
//        return ResponseHelper.ok(produkService.updateProduk(id, produk.getNama(), produk.getImg(), produk.getDeskripsi(), produk.getHarga()));
//    }
    @DeleteMapping("/{id}")
    public void deleteProduk(@PathVariable("id") Integer id) {
        produkService.deleteProduk(id);
    }
}
//    @PostMapping
//    public CommonResponse<Produk> addProduk(@RequestBody Produk produk) {
//        return ResponseHelper.ok(produkService.addProduk(produk));
//    }

