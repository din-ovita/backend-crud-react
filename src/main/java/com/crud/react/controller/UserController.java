package com.crud.react.controller;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.UserDto;
import com.crud.react.model.User;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponseHelper.ok(userService.login(loginDto)); //login
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<User> addUser(UserDto userDto,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(userService.addUser(modelMapper.map(userDto, User.class), multipartFile)); //tambah user (registrasi)
    }

    @GetMapping("/{id}")
    public CommonResponse<User> getById(@PathVariable Integer id) {
        return ResponseHelper.ok(userService.getById(id));
    }

    @GetMapping("/all-user")
    public CommonResponse<List<User>> getAllUser() {
        return ResponseHelper.ok(userService.getAllUser());
    }

    @PutMapping(path="/{id}", consumes = "multipart/form-data")
    public CommonResponse<User> updateUser(@PathVariable("id") Integer id,UserDto user,@RequestPart("file") MultipartFile multipartFile2) {
        return ResponseHelper.ok(userService.updateUser(id, modelMapper.map(user, User.class), multipartFile2));
    }

//    @PutMapping("/update")
//    public CommonResponse<User> updateUser(@RequestParam Integer id, String email, String password, String nama, String alamat,Long noTelepon) {
//        return ResponseHelper.ok(userService.updateUser(id, email, password, nama, alamat, noTelepon));
//    }

    @PutMapping( path="/update/{id}", consumes = "multipart/form-data")
    public CommonResponse<User> updateImg(@PathVariable("id") Integer id, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(userService.updateImg(id, multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Integer id) {
        userService.deleteUser(id);
    }
}


