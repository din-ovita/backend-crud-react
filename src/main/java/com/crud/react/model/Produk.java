package com.crud.react.model;

import javax.persistence.*;

@Entity
@Table(name = "produk")
public class Produk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Lob
    @Column(name = "image")
    private String img;

    @Column(name = "nama")
    private String nama;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "harga")
    private Float harga;

    public Produk() {
    }

    public Produk(String img, String nama, String deskripsi, Float harga) {
        this.img = img;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Float getHarga() {
        return harga;
    }

    public void setHarga(Float harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Produk{" +
                "id=" + id +
                ", img='" + img + '\'' +
                ", nama='" + nama + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", harga=" + harga +
                '}';
    }
}
