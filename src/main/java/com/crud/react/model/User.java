package com.crud.react.model;

import com.crud.react.enumating.UserType;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto_profil")
    private String fotoProfil;

    @Column(name = "no_telepon")
    private String noTelepon;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserType role;

    public User() {
    }

    public User(String email, String password, String nama, String alamat, String fotoProfil, String noTelepon, UserType role) {
        this.email = email;
        this.password = password;
        this.nama = nama;
        this.alamat = alamat;
        this.fotoProfil = fotoProfil;
        this.noTelepon = noTelepon;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFotoProfil() {
        return fotoProfil;
    }

    public void setFotoProfil(String fotoProfil) {
        this.fotoProfil = fotoProfil;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", nama='" + nama + '\'' +
                ", alamat='" + alamat + '\'' +
                ", fotoProfil='" + fotoProfil + '\'' +
                ", noTelepon=" + noTelepon +
                ", role=" + role +
                '}';
    }
}