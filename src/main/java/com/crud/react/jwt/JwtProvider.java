package com.crud.react.jwt;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.TemporaryToken;
import com.crud.react.model.User;
import com.crud.react.repository.TemporaryTokenRepository;
import com.crud.react.repository.UserRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String secretKey = "belajar spring";
    private static Integer expired = 900000;
    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;
    @Autowired
    private UserRepository userRepository;
    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        User user = userRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found Generate token"));
        var checkingToken = temporaryTokenRepository.findByUserId(user.getId());
        if (checkingToken.isPresent()) temporaryTokenRepository.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }
    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }
    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }

//    public String generateToken(UserDetails userDetails) {
//        return Jwts.builder()
//                .setSubject(userDetails.getUsername())
//                .setIssuedAt(new Date())
//                .setExpiration(new Date(new Date().getTime() + expired))
//                .signWith(SignatureAlgorithm.HS512, secretKey)
//                .compact();
//    }

//    public String getSubject(String token) {
//        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
//    }


//    public boolean checkingTokenJwt(String token) {
//        try {
//            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
//            return true;
//        } catch (SignatureException e) {
//            System.out.println("Invalid JWT signature -> Message: {} ");
//        } catch (MalformedJwtException e) {
//            System.out.println("Invalid JWT token -> Message: {} ");
//        } catch (ExpiredJwtException e ) {
//            System.out.println("Expired JWT token -> Message: {} ");
//        } catch (UnsupportedJwtException e ) {
//            System.out.println("Unsupport JWT token -> Message: {} ");
//        } catch (IllegalArgumentException e) {
//            System.out.println("JWT claims string is empty -> Message: {} ");
//        }
//        return false;
//    }
}
