package com.crud.react.service;

import com.crud.react.model.Produk;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProdukService {
    Produk addProduk (Produk produk, MultipartFile multipartFile);
    Produk getProdukId (Integer id);
    Produk updateProduk(Integer id, Produk produk, MultipartFile multipartFile);
    //    Produk updateProduk(Integer id, String nama, String img, String deskripsi, Float harga);
    Page<Produk> getAllProduk (int page, String nama);
    void deleteProduk (Integer id);
}
