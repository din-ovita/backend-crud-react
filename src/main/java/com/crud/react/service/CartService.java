package com.crud.react.service;

import com.crud.react.dto.CartDto;
import com.crud.react.model.Cart;

import java.util.List;
import java.util.Map;

public interface CartService {
    Cart addCart(CartDto cart);
    List<Cart> getAllCart(int userId);
    Cart update(int id, CartDto cart);
    Map<String, Object> delete(int id);
    Map<String, Boolean> checkout(int userId);

}
