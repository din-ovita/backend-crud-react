package com.crud.react.service;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.UserDto;
import com.crud.react.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UserService {
    Map<String, Object> login(LoginDto loginDto);
    User addUser (User user, MultipartFile multipartFile);
    User getById (Integer id);
    User updateUser (Integer id, User user, MultipartFile multipartFile2);
    //    User updateUser (Integer id, String email, String password, String nama, String alamat, Long noTelepon);
    User updateImg (Integer id, MultipartFile multipartFile);
    List<User> getAllUser();
    void deleteUser(Integer id);
}
