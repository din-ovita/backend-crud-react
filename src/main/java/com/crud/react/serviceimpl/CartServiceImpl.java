package com.crud.react.serviceimpl;

import com.crud.react.dto.CartDto;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Cart;
import com.crud.react.model.Produk;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.ProdukRepository;
import com.crud.react.repository.UserRepository;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//handle logic data
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProdukRepository produkRepository;

    @Override
    public Cart addCart(CartDto cart) {
        Produk produk = produkRepository.findById(cart.getProdukId()).orElseThrow(() -> new NotFoundException("Produk id tidak ditemukan"));
        Cart addCart = new Cart();
        addCart.setQuantity(cart.getQuantity());
        addCart.setUser(userRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        addCart.setProduk(produk);
        addCart.setTotalPrice(produk.getHarga() * cart.getQuantity());
        return cartRepository.save(addCart);
    }

    @Override
    public List<Cart> getAllCart(int userId) {
//        User user = userRepository.findById(userId.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan"));
//        Pageable pageable = PageRequest.of(page, 5);
//        List<Cart> produkList = cartRepository.checkout(userId);
//        cartRepository.deleteAll(produkList);
        return cartRepository.findByUser(userId);
    }

    @Override
    public Cart update(int id, CartDto cart) {
        Cart data = cartRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        data.setQuantity(cart.getQuantity());
        data.setUser(userRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        data.setProduk(produkRepository.findById(cart.getProdukId()).orElseThrow(() -> new NotFoundException("Produk id tidak ditemukan")));
        return cartRepository.save(data);
    }

    @Override
    public Map<String, Object> delete(int id) {
        cartRepository.deleteById(id);
        Map<String, Object> obj = new HashMap<>();
        obj.put("DELETE", true);
        return obj;
    }

    @Override
    public Map<String, Boolean> checkout(int userId) {
        var produkList = cartRepository.findByUser(userId);
        cartRepository.deleteAll(produkList);
        Map<String, Boolean> obj = new HashMap<>();
        obj.put("DELETE", Boolean.TRUE);
        return obj;
    }

 }