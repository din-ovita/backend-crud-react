package com.crud.react.serviceimpl;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Produk;
import com.crud.react.repository.ProdukRepository;
import com.crud.react.service.ProdukService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.directory.SearchControls;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProdukServiceImpl implements ProdukService {
    @Autowired
    ProdukRepository produkRepository;

//    @Override
//    public Produk addProduk(Produk produk) {
//        return produkRepository.save(produk);
//    }

//  FIREBASE
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-product.appspot.com/o/%s?alt=media";
    @Override
    public Produk addProduk(Produk produk, MultipartFile multipartFile) {
//        Produk produk1 = produkRepository.save(produk);
        String url = imageConverter(multipartFile);
        Produk produkImage = new Produk(url, produk.getNama(), produk.getDeskripsi(), produk.getHarga());
        return produkRepository.save(produkImage);
    }
//    untuk sistem utama, menggunakan upload sistem ke fire base, multipartfile
    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtensions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }
    ////    merubah multipartfile menjadi file
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    ////    mengupload file
    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-product.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
//        OTOMATIS NAMBAH FILE, memanggil nama file
    private String getExtensions(String fileName) {
        return fileName.split("\\.")[0];
    }

    @Override
    public Produk getProdukId (Integer id) {
        return produkRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    } //METHOD GET

    @Override
    public Produk updateProduk (Integer id, Produk produk, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        Produk produk1 = produkRepository.findById(id).get();
        produk1.setNama(produk.getNama());
        produk1.setImg(url);
        produk1.setDeskripsi(produk.getDeskripsi());
        produk1.setHarga(produk.getHarga());
        return produkRepository.save(produk1);
    } //METHOD PUT


    //    @Override
//    public Produk updateProduk (Integer id, String nama, String img, String deskripsi, Float harga) {
//        Produk produk = produkRepository.findById(id).get();
//        produk.setNama(nama);
//        produk.setImg(img);
//        produk.setDeskripsi(deskripsi);
//        produk.setHarga(harga);
//        return produkRepository.save(produk);
//    } //METHOD PUT
    @Override
    public Page<Produk> getAllProduk(int page, String nama) {
        Pageable pages = PageRequest.of(page, 5);
        return produkRepository.findByName(nama, pages);
    } //METHOD GET ALL
    @Override
    public void deleteProduk(Integer id) {
        produkRepository.deleteById(id);
    } //METHOD DELETE
}
