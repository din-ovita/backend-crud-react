package com.crud.react.serviceimpl;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.UserDto;
import com.crud.react.enumating.UserType;
import com.crud.react.exception.EmailCondition;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Produk;
import com.crud.react.model.User;
import com.crud.react.repository.UserRepository;
import com.crud.react.service.UserService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
//    @Override
//    public User addUser(User user) {
//        if (userRepository.findByEmail(user.getEmail()).isPresent()) throw new EmailCondition("Email already exist");
//
//        return userRepository.save(user);
//    }

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-product.appspot.com/o/%s?alt=media";

    @Override
    public User addUser(User user, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        String email = user.getEmail();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (user.getRole().name().equals("ADMIN"))
            user.setRole(UserType.ADMIN);
        else user.setRole(UserType.USER);
        var validasi = userRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        user.getNama();
        user.getAlamat();
        user.getNoTelepon();
        user.setFotoProfil(url);
        return userRepository.save(user);
    }

    //    @Override
//    public User updateUser (Integer id, String email, String password) {
//        User user = userRepository.findById(id).get();
//        user.setEmail(email);
//        user.setPassword(password);
//        return userRepository.save(user);
//    }

    @Override
    public User updateImg(Integer id, MultipartFile multipartFile) {
        User gambar = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        String url = imageConverter(multipartFile);
//                user.setFotoProfil(url);
        gambar.setFotoProfil(url);
        return userRepository.save(gambar);
    }

    @Override
    public User getById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

//    @Override
//    public User updateUser(Integer id, String email, String password, String nama, String alamat,Long noTelepon) {
//        User update = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
//        var validasi = userRepository.findByEmail(email);
//        if (validasi.isPresent()) {
//            throw new InternalErrorException("Maaf email sudah ada");
//        }
//        update.setEmail(email);
//        update.setPassword(password);
//        update.setNama(nama);
//        update.setAlamat(alamat);
//        update.setNoTelepon(noTelepon);
//        return userRepository.save(update);
//    }


    @Override
    public User updateUser(Integer id, User user, MultipartFile multipartFile2) {
        String image = imageConverter(multipartFile2);
        User update = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        var validasi = userRepository.findByEmail(user.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email sudah ada");
        }
        update.setEmail(user.getEmail());
        update.setPassword(passwordEncoder.encode(user.getPassword()));
        update.setNama(user.getNama());
        update.setAlamat(user.getAlamat());
        update.setNoTelepon(user.getNoTelepon());
        update.setFotoProfil(image);
        update.setRole(user.getRole());
        return userRepository.save(update);
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtensions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-product.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    private String getExtensions(String fileName) {
        return fileName.split("\\.")[0];
    }


    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }
    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    private  String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        User user = userRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", user);
        return response;
    }
}
